from flask import Blueprint, redirect, render_template, url_for, \
     current_app, g, request
from datetime import datetime
from flask_babel import _
from ..auth import login_required, moderator_required
from ..models import Post, Hashtag
from ..images import get_image
from ..tags import get_tags
from ..main import db



bp = Blueprint('posts', __name__)


@bp.route('/')
@login_required
def index():
    return render_template('posts/index.html', 
        posts=Post.query.filter(Post.approved != None)
			.order_by(Post.approved.desc()).limit(2),
        feed='all', title='Posts'
    )


@bp.route('/approve')
@login_required
def approve():
    assert g.user.is_admin or g.user.is_moderator
    return render_template('posts/index.html', 
        posts=Post.query.filter(Post.approved == None)
			.order_by(Post.added.desc()).limit(2),
        feed='approve', title=_('Approve')
    )


@bp.route('/yours')
@login_required
def yours():
    return render_template('posts/index.html', 
        posts=Post.query.filter_by(author_id=g.user.id)
			.order_by(Post.added.desc()).limit(2),
        feed='yours', title=_('Your posts')
    )


@bp.route('/new', methods=['GET', 'POST'])
@login_required
def new(): 
    return render_template('posts/editor.html')


@bp.route('/edit/<int:post_id>')
@login_required
def edit(post_id):
    post = Post.query.filter_by(id=post_id).one()
    return render_template('posts/editor.html', post=post)

