import traceback
from flask import current_app
import requests
from .main import db
from .models import Notification, User


def send_notification(receiver_id, type_):
    cation = Notification(receiver_id=receiver_id, type=type_)
    db.session.add(cation)
    receiver = User.query.filter_by(id=receiver_id).one()

    push_server = current_app.config.get('PUSH_SERVER_HTTP', None)

    if push_server:
        try:
            response = requests.post(push_server + '/send', json={
                'receiver': receiver.username,
                'notification_amount': receiver.notification_amount,
                'text': cation.text
            })
            response.raise_for_status()
        except:
            traceback.print_exc()

        
