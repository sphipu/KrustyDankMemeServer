from functools import wraps
import hashlib
from flask import request, g, url_for, current_app, abort
from .models import User, Session



def hash_password(username, password):
    """Get the passwordhash for a and plain password"""
    return hashlib.sha256((username + password).encode('utf8')).hexdigest()


def get_session():
    """Get the session or None, if no valid token was found"""
    if hasattr(g, 'session'):
        return g.session

    session_token = request.cookies.get('krusty-session')
    if session_token:
        return Session.query.filter_by(token=session_token).first()
        

def moderator_required(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        if g.user.is_moderator or g.user.is_admin:
            return f(*args, **kwds)

        return abort(403)
    return wrapper


def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        session = get_session()
        if session:
            g.user = session.user
            g.session = session
            return f(*args, **kwds)

        return abort(403)
    return wrapper

