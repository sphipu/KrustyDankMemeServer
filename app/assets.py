import os.path
import hashlib
from functools import lru_cache
from flask import url_for, g, send_file, jsonify, current_app, abort
from flask_wtf.csrf import generate_csrf
import slimit
import csscompressor
from .models import Post


def asset_compiler_none(src):
    return src


ASSET_COMPILERS = {
    '.js': lambda src: slimit.minify(src.decode(), mangle=True).encode('utf-8'),
    '.css': lambda src: csscompressor.compress(src.decode()).encode('utf-8')
}


def manifest():
    return jsonify({
      "short_name": "Krusty",
      "name": "Krusty dank meme",
      "icons": [
        {
          "src": url_for('static', filename='img/logo-192.png'),
          "type": "image/png",
          "sizes": "192x192"
        },
        {
          "src": url_for('static', filename='img/logo-512.png'),
          "type": "image/png",
          "sizes": "512x512"
        }
      ],
      "start_url": "/?source=pwa",
      "background_color": "#000000",
      "display": "standalone",
      "scope": "/",
      "theme_color": "#FFDF00"
    })


def get_app_props():
    return {
        "ajaxEndpoints": {
            "ajax.like_post": url_for('ajax.like_post'),
            "ajax.like_comment": url_for('ajax.like_comment'),
            "ajax.logout": url_for('ajax.logout'),
            "ajax.approve_post": url_for('ajax.approve_post'),
            "ajax.add_comment": url_for('ajax.add_comment'),
            "ajax.delete_post": url_for('ajax.delete_post'),
            "ajax.get_posts": url_for('ajax.get_posts'),
            "ajax.subscribe_user": url_for('ajax.subscribe_user'),
            "ajax.save_post": url_for('ajax.save_post'),
            "ajax.read_notification": url_for('ajax.read_notification')
        },
        "csrfToken": generate_csrf(),
        "notificationAmount": g.user.notification_amount if hasattr(g, 'user') else 0,
        "approvablePostAmount": Post.query.filter(Post.approved.is_(None)).count(),
        "pushServer": current_app.config.get('PUSH_SERVER_WS'),
        "pushToken": g.session.token if hasattr(g, 'session') else False
    }


def filehash(fname):
    md5 = hashlib.md5()
    with open(fname, 'rb') as f:
        buf = f.read(4096)
        while buf:
            md5.update(buf)
            buf = f.read(4096)
    return md5.hexdigest()


def get_fullname(src_fname):
    """Get the full name of the source and the destination file"""
    src_fullname = os.path.join(current_app.root_path, 'assets', src_fname)
    fhash = filehash(src_fullname)
    name, ext = os.path.splitext(os.path.basename(src_fname))
    dst_basename = '%s-%s.%s' % (name, fhash, ext[1:])
    dst_fullname = os.path.join(current_app.instance_path, 'cache', dst_basename)
    
    return src_fullname, dst_fullname


def compile_asset(src_fullname, dst_fullname):
    if os.path.isfile(dst_fullname):
        return  # do nothing if the file is already compiled

    if current_app.config['DEBUG']:
        fn = asset_compiler_none
        print('.. no debug ..')
    else:
        _, ext = os.path.splitext(src_fullname)
        fn = ASSET_COMPILERS.get(ext, asset_compiler_none)

    with open(src_fullname, 'rb') as src:
        compiled = fn(src.read())
    
    with open(dst_fullname, 'wb') as dst:
        dst.write(compiled)


@lru_cache(64)
def get_asset_basename(fname):
    src_fullname, dst_fullname = get_fullname(fname)
    compile_asset(src_fullname, dst_fullname)
    return os.path.basename(dst_fullname)


def get_asset_url(fname):
    if current_app.config['DEBUG']:
        basename = get_asset_basename.__wrapped__(fname)
    else:
        # Bypass cache
        basename = get_asset_basename(fname)
    return url_for('assets', bn=basename)


def assets(bn):
    fullname = os.path.join(current_app.instance_path, 'cache', bn)
    if not os.path.isfile(fullname):
        return abort(404)
    return send_file(fullname)


def app_register_assets(app):
    app.jinja_env.globals['asset_url'] = get_asset_url
    app.jinja_env.globals['manifest_url'] = lambda: url_for('manifest')
    app.route('/assets/manifest.json')(manifest)
    app.route('/assets/<string:bn>')(assets)
