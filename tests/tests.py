API_URL = 'http://localhost:5000/api/v1.0/'
ADMIN_USER = 'admin'
ADMIN_PASSWORD = 'passw0rd'

USER_DEFAULTS = {
    'is_enabled': True,
    'is_admin': False,
    'first_name': 'Test',
    'last_name': 'User',
    'address': 'Testenvironment',
    'zipcode': 127001,
    'town': 'Localhost',
}


import os
import sys
import unittest
import uuid
import requests

CURRENT_PATH = os.path.abspath(os.path.dirname(__file__))

if not os.path.isdir(os.path.join(CURRENT_PATH, 'tmp')):
    os.mkdir(os.path.join(CURRENT_PATH, 'tmp'))


# Helper functions
def get_url(resource):
    if API_URL.endswith('/'):
        url = API_URL[:-1]
    else:
        url = API_URL
    
    if resource.startswith('/'):
        res = resource[1:]
    else:
        res = resource

    return '%s/%s' % (url, res)


def get_random_user():
    return 'testuser_' + str(uuid.uuid4()), str(uuid.uuid4())



def create_user(username, password, image_id=None, invite=None):
    url = get_url('/users')
    user = USER_DEFAULTS.copy()
    user.update({
        'username': username,
        'password': password,
        'email': '{}@localhost.test'.format(username)
    })

    if image_id:
        user['image_id'] = image_id

    if invite:
        user['invite'] = invite

    resp = requests.post(url, json=user)
    resp.raise_for_status()


def delete_user(token):
    # Delete the user
    url = get_url('/users')
    resp = requests.delete(url, headers=get_auth_header(token))
    resp.raise_for_status()


def get_token(username, password):
    resp = requests.get(get_url('/tokens'), auth=(username, password))
    resp.raise_for_status()
    return resp.json()['token']


def get_imageurl(image_id):
    return get_url('/images/%s' % image_id)


def get_auth_header(token):
    return {'Authorization': 'Token {}'.format(token)}

# Test cases


class TestImages(unittest.TestCase):
    def setUp(self):
        username, password = get_random_user()
        create_user(username, password)
        self.token = get_token(username, password)

    def tearDown(self):
        delete_user(self.token)

    def upload_image(self, image_id, src_path):
        with open(src_path, 'rb') as f:
            resp = requests.post(get_imageurl(image_id), data=f.read(), headers=get_auth_header(self.token))
            resp.raise_for_status()
    
    def get_image(self, image_id, dst_path):
        with open(dst_path, 'wb') as f:
            resp = requests.get(get_imageurl(image_id), headers=get_auth_header(self.token))
            resp.raise_for_status()
            f.write(resp.content)
    
    def delete_image(self, image_id):
        resp = requests.delete(get_imageurl(image_id), headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def get_metadata(self, image_id):
        resp = requests.get(get_imageurl(image_id) + '/metadata', headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()

    def test_image(self):
        image_id = str(uuid.uuid4())
        self.upload_image(image_id, os.path.join(CURRENT_PATH, 'testimages/big.jpg'))
        self.get_image(image_id, os.path.join(CURRENT_PATH, 'tmp/') + image_id)
        self.delete_image(image_id)
    

class TestInvites(unittest.TestCase):
    def setUp(self):
        username, password = get_random_user()
        create_user(username, password)
        self.token = get_token(username, password)
    
    def tearDown(self):
        delete_user(self.token)

    def get_invite(self):
        url = get_url('/invites')
        resp = requests.get(url, headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()['invite']
    
    def use_invite(self, invite, username, password):
        create_user(username, password, invite=invite)
    
    def is_invite_used(self, invite: str) -> bool:
        resp = requests.get(get_url('/invites/' + invite))
        resp.raise_for_status()
        return resp.json()['user'] is not None
    
    def test_invite(self):
        invite = self.get_invite()
        self.assertFalse(self.is_invite_used(invite))
        invited_user = 'invited_%s' % str(uuid.uuid4())
        password = 'passw0rd'
        self.use_invite(invite, invited_user, password)
        self.assertTrue(self.is_invite_used(invite))
        token = get_token(invited_user, password)
        delete_user(token)


class PostTests(unittest.TestCase):
    def setUp(self):
        username, password = get_random_user()
        create_user(username, password)
        self.token = get_token(username, password)
        self.category = 'TestCategory_' + password
        self.username = username
    
    def tearDown(self):
        delete_user(self.token)
        pass
    
    def create_category(self, name):
        category = {'name': name}
        resp = requests.post(get_url('/categories'),
                             json=category,
                             headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def category_exists(self, name):
        resp = requests.get(get_url('/categories'),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        self.assertIn(name, resp.json())
    
    def delete_category(self, category):
        resp = requests.delete(get_url('/categories/{}'.format(category)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()

    def upload_image(self, image_id, file):
        resp = requests.post(get_url('/images/{}'.format(image_id)),
                             data=file.read(),
                             headers=get_auth_header(self.token))
        resp.raise_for_status()

    def create_post(self, image_id, category):
        post = {
            'image_id': image_id,
            'comment': 'Test comment... #tag1 #tag2 @%s' % ADMIN_USER,
            'category': category,
        }
        resp = requests.post(get_url('/posts'),
                             json=post,
                             headers=get_auth_header(self.token))
        resp.raise_for_status()

    def approve_post(self, post_id):
        resp = requests.post(get_url('/posts/{}/approved'.format(post_id)),
                             headers=get_auth_header(self.token),
                             json={'approve': True})
        resp.raise_for_status()
    
    def modify_post(self, post_id):
        resp = requests.put(get_url('/posts/{}'.format(post_id)),
                            headers=get_auth_header(self.token),
                            json={'comment': 'Test comment... #tag3 @%s' % ADMIN_USER})
        resp.raise_for_status()


    def get_unapproved(self, category):
        resp = requests.get(get_url('/posts/category/{}/unapproved'.format(category)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()

    def delete_post(self, post_id):
        resp = requests.delete(get_url('/posts/{}'.format(post_id)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()

    def post_exists_list(self, post_id, category):
        resp = requests.get(get_url('/posts/category/{}'.format(category)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        for post in resp.json():
            if post['post_id'] == post_id:
                return True
        return False

    def post_user_exists(self, username, post_id):
        resp = requests.get(get_url('/posts/user/{}'.format(username)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        for post in resp.json():
            if post['post_id'] == post_id:
                return True
        return False
    
    def create_comment(self, post_id):
        comment = {'text': 'This is a test comment...'}
        resp = requests.post(get_url('/comments/post/{}'.format(post_id)),
                             headers=get_auth_header(self.token),
                             json=comment)
        resp.raise_for_status()
        
    def get_comments(self, post_id):
        resp = requests.get(get_url('/comments/post/{}'.format(post_id)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()

    def modify_comment(self, comment_id):
        resp = requests.put(get_url('/comments/{}'.format(comment_id)),
                            headers=get_auth_header(self.token),
                            json={'text': 'Modified comment'})
        resp.raise_for_status()
    
    def answer_comment(self, comment_id):
        resp = requests.post(get_url('/comments/answer/{}'.format(comment_id)),
                             headers=get_auth_header(self.token),
                             json={'text': 'The answer to a comment...'})
        resp.raise_for_status()
    
    def delete_comment(self, comment_id):
        resp = requests.delete(get_url('/comments/{}'.format(comment_id)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def create_post_reaction(self, post_id):
        resp = requests.put(get_url('/reactions/post/{}'.format(post_id)),
                            headers=get_auth_header(self.token),
                            json={'reaction_type': 1})
        resp.raise_for_status()
    
    def create_comment_reaction(self, comment_id):
        resp = requests.put(get_url('/reactions/comment/{}'.format(comment_id)),
                            headers=get_auth_header(self.token),
                           json={'reaction_type': 1})
        resp.raise_for_status()
    
    def delete_post_reaction(self, post_id):
        resp = requests.delete(get_url('/reactions/post/{}'.format(post_id)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def delete_comment_reaction(self, comment_id):
        resp = requests.delete(get_url('/reactions/comment/{}'.format(comment_id)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def get_post_reactions(self, post_id):
        resp = requests.get(get_url('/reactions/post/{}'.format(post_id)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()

    def get_comment_reactions(self, comment_id):
        resp = requests.get(get_url('/reactions/comment/{}'.format(comment_id)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()

    def get_post_reactions_amount(self, post_id):
        resp = requests.get(get_url('/reactions/post/{}/count'.format(post_id)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()['reactions']

    def get_comment_reactions_amount(self, comment_id):
        resp = requests.get(get_url('/reactions/comment/{}/count'.format(comment_id)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()['reactions']

    def tagged_post_exists(self, tag, post_id):
        resp = requests.get(get_url('/posts/tag/{}'.format(tag)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        for i in resp.json():
            if i['post_id'] == post_id:
                return True
        return False

    def mentioned_post_exists(self, user, post_id):
        resp = requests.get(get_url('/posts/mention/{}'.format(user)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        for i in resp.json():
            if i['post_id'] == post_id:
                return True
        return False

    def pin_post(self, post_id):
        resp = requests.post(get_url('/posts/pinned'),
                             headers=get_auth_header(self.token),
                             json={'post_id': post_id})
        resp.raise_for_status()

    def pinned_post_exists(self, username, post_id):
        resp = requests.get(get_url('/posts/pinned/{}'.format(username)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        for i in resp.json():
            if i['post_id'] == post_id:
                return True
        return False

    def delete_pinned_post(self, post_id):
        resp = requests.delete(get_url('/posts/pinned/{}'.format(post_id)),
                                headers=get_auth_header(self.token))
        resp.raise_for_status()

    def subscribe_category(self, category):
        resp = requests.post(get_url('/categories/{}/subscribed'.format(category)),
                             headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def unsubscribe_category(self, category):
        resp = requests.delete(get_url('/categories/{}/subscribed'.format(category)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def is_category_subscribed(self, category):
        resp = requests.get(get_url('/categories/{}/subscribed'),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()['subscribed']

    def subscribe_user(self, username):
        resp = requests.post(get_url('/users/{}/subscribed'.format(username)),
                             headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def unsubscribe_user(self, username):
        resp = requests.delete(get_url('/users/{}/subscribed'.format(username)),
                               headers=get_auth_header(self.token))
        resp.raise_for_status()
    
    def is_user_subscribed(self, username):
        resp = requests.get(get_url('/users/{}/subscribed'.format(username)),
                            headers=get_auth_header(self.token))
        resp.raise_for_status()
        return resp.json()['subscribed']

    def is_post_in_feed(self, post_id):
        resp = requests.get(get_url('/posts/feed'), headers=get_auth_header(self.token))
        resp.raise_for_status()
        for post in resp.json():
            if post['post_id'] == post_id:
                return True
        return False
    
    def test_post(self):
        category = self.category
        image_id = str(uuid.uuid4())
        self.create_category(category)
        with open(os.path.join(CURRENT_PATH, 'testimages/big.jpg'), 'rb') as fp:
            self.upload_image(image_id, fp)
        self.create_post(image_id, category)
        post_id = self.get_unapproved(category)[0]['post_id']
        self.assertFalse(self.post_exists_list(post_id, category))
        self.assertFalse(self.post_user_exists(self.username, post_id))
        self.assertFalse(self.tagged_post_exists('tag1', post_id))
        self.assertFalse(self.mentioned_post_exists(ADMIN_USER, post_id))
        self.approve_post(post_id)
        self.assertTrue(self.post_exists_list(post_id, category))
        self.assertTrue(self.post_user_exists(self.username, post_id))
        self.assertTrue(self.tagged_post_exists('tag1', post_id))
        self.assertTrue(self.mentioned_post_exists(ADMIN_USER, post_id))
        self.modify_post(post_id)
        self.assertFalse(self.tagged_post_exists('tag1', post_id))
        self.assertTrue(self.tagged_post_exists('tag3', post_id))
        self.assertTrue(self.mentioned_post_exists(ADMIN_USER, post_id))
        self.delete_post(post_id)
    
    def test_comments(self):
        category = self.category
        image_id = str(uuid.uuid4())
        self.create_category(category)
        with open(os.path.join(CURRENT_PATH, 'testimages/big.jpg'), 'rb') as fp:
            self.upload_image(image_id, fp)
        self.create_post(image_id, category)
        post_id = self.get_unapproved(category)[0]['post_id']
        self.create_comment(post_id)
        comment_id = self.get_comments(post_id)[0]['comment_id']
        self.answer_comment(comment_id)
        self.delete_comment(comment_id)

    def test_reactions(self):
        category = self.category
        image_id = str(uuid.uuid4())
        self.create_category(category)
        with open(os.path.join(CURRENT_PATH, 'testimages/small.jpg'), 'rb') as fp:
            self.upload_image(image_id, fp)
        self.create_post(image_id, category)
        post_id = self.get_unapproved(category)[0]['post_id']
        self.create_comment(post_id)
        comment_id = self.get_comments(post_id)[0]['comment_id']
        self.create_post_reaction(post_id)
        self.create_comment_reaction(comment_id)
        self.assertEqual(self.get_post_reactions(post_id)[0]['reaction_type'], 1)
        self.assertEqual(self.get_comment_reactions(comment_id)[0]['reaction_type'], 1)
        self.assertEqual(self.get_post_reactions_amount(post_id), 1)
        self.assertEqual(self.get_comment_reactions_amount(comment_id), 1)
        self.delete_post_reaction(post_id)
        self.delete_comment_reaction(comment_id)
    
    def test_profile_picture(self):
        username = self.username + '_profile_image'
        image_id = str(uuid.uuid4())
        password = image_id
        with open(os.path.join(CURRENT_PATH, 'testimages/small.jpg'), 'rb') as fp:
            self.upload_image(image_id, fp)
        create_user(username, password, image_id)
        token = get_token(username, password)
        delete_user(token)


class TestUser(unittest.TestCase):
    def create_user(self, is_admin=False, url=None, headers=None):
        self.username = 'testuser_' + str(uuid.uuid4())
        self.password = 'passw0rd_' + str(uuid.uuid4())

        # Create user
        if not url:
            url = get_url('/users')

        user = {
            'username': self.username,
            'password': self.password,
            'is_enabled': True,
            'is_admin': is_admin,
            'first_name': 'Test',
            'last_name': 'User',
            'address': 'Testenvironment',
            'zipcode': 127001,
            'town': 'Localhost',
            'email': '{}@localhost'.format(self.username)
        }

        resp = requests.post(url, json=user, headers=headers)
        resp.raise_for_status()
    


    def get_token(self, username=None, password=None):
        # Get a token for that user
        if not username:
            username = self.username
        if not password:
            password = self.password

        url = get_url('/tokens')
        resp = requests.get(url, auth=(username, password))
        resp.raise_for_status()
        self.headers = {
            'Authorization': 'Token ' + resp.json()['token']
        }
            
    def modify_user(self):
        # Modify the user (address, zipcode)
        url = get_url('/users')
        new_values = {
            'address': 'Heaven',
            'zipcode': 4242
        }

        resp = requests.put(url, json=new_values, headers=self.headers)
        resp.raise_for_status()
    
    def modify_to_admin(self, expect_forbidden=True, is_admin=True, url=None):
        if not url:
            url = get_url('/users')
        new_values = {'is_admin': is_admin}
        resp = requests.put(url, json=new_values, headers=self.headers)
        if expect_forbidden:
            self.assertEqual(resp.status_code, 403)
        else:
            resp.raise_for_status()

    def get_user(self):
        # Get the userprofile
        url = get_url('/users/' + self.username)
        resp = requests.get(url, headers=self.headers)
        resp.raise_for_status()
        return resp.json()
    
    def check_user_modified(self, user):
        self.assertEqual(user['address'], 'Heaven')
        self.assertEqual(user['zipcode'], 4242)

    def check_is_admin(self, user, expected=False):
        self.assertEqual(user['username'], self.username)
        self.assertEqual(user['is_admin'], expected)

    def delete_user(self, url=None):
        # Delete the user
        if not url:
            url = get_url('/users')
        resp = requests.delete(url, headers=self.headers)
        resp.raise_for_status()
        self.headers = None
    
    def test_create_modify_delete(self):
        self.create_user()
        self.get_token()
        self.modify_user()
        self.check_user_modified(self.get_user())
        self.delete_user()
    
    def test_create_self_admin(self):
        self.create_user(is_admin=True)
        self.get_token()
        self.check_is_admin(self.get_user())
        self.modify_to_admin()
        self.check_is_admin(self.get_user())
        self.delete_user()
    
    def delete_token(self):
        resp = requests.delete(get_url('/tokens'), headers=self.headers)
        resp.raise_for_status()
    
    def test_create_admin(self):
        self.get_token(ADMIN_USER, ADMIN_PASSWORD)
        self.create_user(is_admin=True, url=get_url('/users/admin'), headers=self.headers)
        modify_url = get_url('/users/' + self.username)
        user = self.get_user()
        self.assertEqual(user['is_admin'], True)
        self.modify_to_admin(expect_forbidden=False, is_admin=False, url=modify_url)
        user = self.get_user()
        self.assertEqual(user['is_admin'], False)
        self.modify_to_admin(is_admin=True, expect_forbidden=False, url=modify_url)
        user = self.get_user()
        self.assertEqual(user['is_admin'], True)
        self.delete_user(url=modify_url)
    
    def test_tokens(self):
        self.get_token(ADMIN_USER, ADMIN_PASSWORD)
        self.delete_token()
    

    if __name__ == '__main__':
        unittest.main()