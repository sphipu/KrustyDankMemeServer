from flask import Blueprint, redirect, render_template, url_for, g, request
from datetime import datetime, timedelta
from ..auth import hash_password, login_required
from ..models import User, Session
from ..forms import UserForm, LoginForm, ProfileForm, EditUserForm
from ..users import copy_user_in_form, copy_form_in_user
from ..images import get_image, save_image
from ..main import db


bp = Blueprint('users', __name__)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        response = redirect(url_for('posts.index'))
        user = User.query.filter_by(
            username=form.username.data,
            password_hash=hash_password(form.username.data, form.password.data)
        ).one()
        user.last_login = datetime.utcnow()
        session = Session(user=user)
        
        db.session.add(user)
        db.session.add(session)
        db.session.commit()
        expires = datetime.utcnow() + timedelta(days=90)
        response.set_cookie('krusty-session', session.token, expires=expires)
        return response

    return render_template('users/login.html', form=form)


@bp.route('/edit_info', methods=['GET', 'POST'])
@login_required
def edit_info():
    form = EditUserForm()

    if form.validate_on_submit():
        user = User.query.filter_by(id=g.user.id).one()
        copy_form_in_user(form, user)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('posts.index'))

    if request.method == 'GET':
        copy_user_in_form(g.user, form)

    return render_template('users/info.html', form=form, mode='edit')


@bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = UserForm()

    if form.validate_on_submit():
        user = User(is_enabled=True)
        copy_form_in_user(form, user)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('users.login'))

    return render_template('users/info.html', form=form, mode='signup')


@bp.route('/profile/<string:username>')
@login_required
def profile_of(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        return 'User not found', 404

    return render_template('users/profile.html', user=user, mode='view')


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    user = g.user
    form = ProfileForm()

    if form.validate_on_submit():
        if form.profile_image.data:
            user.profile_image_name = save_image(form.profile_image.data)

        if form.header_image.data:
            user.header_image_name = save_image(form.header_image.data)

        user.user_info = form.user_info.data
        db.session.add(user)
        db.session.commit()

        return redirect(url_for('posts.index'))

    form.user_info.data = user.user_info

    return render_template('users/profile.html', user=user,
                           form=form, mode='edit')



    

@bp.route('/notifications')
@login_required
def notifications():
    return render_template('users/notifications.html')
