from flask import render_template


def handle_403(err):
    return render_template('errors/403.html'), 403

def handle_404(err):
    return render_template('errors/404.html'), 404

def add_error_handlers(app):
    app.errorhandler(404)(handle_404)
    app.errorhandler(403)(handle_403)
