!function() {
    window.initProfile = function (app, el) {
        app.helpers.ensureInitOnce(el);
        
        var id = el.dataset.userid,
            subscribeButton = el.querySelector(".subscribe-button"),
            subscribeIcon = el.querySelector(".subscribe-button .icon"),
            followerAmount = el.querySelector(".user-stat.follower-amount .amount"),
            profileImage = el.querySelector(".profile-image"),
            headerImage = el.querySelector(".header-image"),
            profileImageInput = el.querySelector(".profile-image-input"),
            headerImageInput = el.querySelector(".header-image-input");
    
        subscribeButton && subscribeButton.addEventListener("click", function() {
            app.tryFetchContent("POST", "ajax.subscribe_user", {"user_id": id}, function(res) {
                followerAmount.innerHTML = res["follower_amount"];
                if (res["is_subscribed"]) {
                    subscribeButton.classList.add("active");
                    subscribeIcon.classList.add("img-checked");
                    subscribeIcon.classList.remove("img-unchecked");
                }
                else {
                    subscribeButton.classList.remove("active");
                    subscribeIcon.classList.remove("img-checked");
                    subscribeIcon.classList.add("img-unchecked");
                }
            });
        });
        
        if (parseInt(el.dataset.editable)) {
            // Allow to change images
            window.initImagePicker(app, profileImage, {
                onChange: function(dataUrl) {
                    profileImageInput.value = dataUrl;
                }
            });
            
            window.initImagePicker(app, headerImage, {
                onChange: function(dataUrl) {
                    headerImageInput.value = dataUrl;
                }
            });
        }
    }
}();
