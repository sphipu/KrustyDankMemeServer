"""Initialize flask (wsgi callable) and some other stuff"""
from os.path import join as join_path
from flask import Flask, redirect, url_for, current_app, g, request
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from flask_migrate import Migrate
from flask_babel import Babel, format_datetime

db = SQLAlchemy()
babel = Babel()
csrf = CSRFProtect()

from .tags import find_tags
from . import ad_tags
#from .apiv10 import api as api_bp
from .views.posts import bp as posts_bp
from .views.images import bp as images_bp
from .views.ajax import bp as ajax_bp
from .views.main import bp as main_bp
from .views.users import bp as users_bp
from .auth import get_session
from .config import config_init_app
from .cli import cli_init_app
from .errors import add_error_handlers
from .assets import get_app_props, app_register_assets


def before_request():
    session = get_session()
    if session:
        g.session = session
        g.user = session.user

@babel.localeselector
def get_locale():
    langs = ['en', 'de']
    return request.accept_languages.best_match(langs)


def create_app():
    app = Flask(__name__)
    config_init_app(app)
    cli_init_app(app)
    app.config.from_pyfile(join_path(app.instance_path, 'settings.py'), True)
    app.config.from_envvar('KRUSTY_SETTINGS', True)
    db.init_app(app)
    babel.init_app(app)
    csrf.init_app(app)
    Migrate(app, db)

    app.jinja_env.globals['find_tags'] = find_tags
    app.jinja_env.globals['ad_tags'] = ad_tags
    app.jinja_env.globals['get_locale'] = get_locale
    app.jinja_env.globals['get_app_props'] = get_app_props
    app.jinja_env.filters['date_s'] = lambda d: format_datetime(d, 'short')
    app.jinja_env.filters['date_f'] = lambda d: format_datetime(d, 'full')
    app.jinja_env.lstrip_blocks = True
    app.jinja_env.trim_blocks = True
    
    #app.register_blueprint(api_bp, url_prefix='/api/v1.0')
    app.register_blueprint(posts_bp, url_prefix='/posts')
    app.register_blueprint(images_bp, url_prefix='/images')
    app.register_blueprint(ajax_bp, url_prefix='/ajax')
    app.register_blueprint(main_bp, url_prefix='/')
    app.register_blueprint(users_bp, url_prefix='/users')
    app_register_assets(app)
    app.before_request(before_request)
    add_error_handlers(app)

    return app
    

