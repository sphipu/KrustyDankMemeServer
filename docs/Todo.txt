+ Allow Web-UI to approve posts
+ Show only approved posts in web-ui
+ Allow comments
+ Allow Web-UI to delete / modify post
+ Allow Web-UI to register
+ Improve login page
+ Emojies are shwed as question marks (mysql must use utf8 encoding)
+ Start on login page, if not logged in (smart redirect from /)
- No case sensitive user name
+ Moderation access rights are not applied correctly
+ Spelling issue on login page
+ Allow Web-UI to show user profiles
+ Password and password confirm field on signup / profile page are not side to side of each other
+ Implement Hashtags / Mentions
+ Allow user to modify its profile
+ Allow to like comments
+ Allow users to load more than 200 posts (a fetch more button)
- Allow users to load more than 200 comments (a fetch more button)
- Allow MP4 Videos
- Custom error pages (Json and HTML)



