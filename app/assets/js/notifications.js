!function() {
    window.initNotification = function(app, el) {
        app.helpers.ensureInitOnce(el, "notification");
        var id = app.helpers.ensureSet(el.dataset.nid),
            checkFrame = el.querySelector(".frame"),
            checkImage = checkFrame.querySelector(".icon");
        
        checkFrame.addEventListener("click", function() {
            var data = {"notification_id": id};
            app.tryFetchContent("POST", "ajax.read_notification", data, function(res) {
                var ecl = el.classList, icl = checkImage.classList;
                if (res["is_read"]) {
                    ecl.add("read");
                    icl.remove("img-unchecked");
                    icl.add("img-checked");
                }
                else {
                    ecl.remove("read");
                    icl.remove("img-checked");
                    icl.add("img-unchecked");
                }
                
                app.dispatchEvent("notification", new CustomEvent("notificationChanged", { detail: {
                    unreadAmount: res["unread_notifications"]
                }}));
            });
        });
    };
}();
