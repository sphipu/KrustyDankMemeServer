from collections import defaultdict
import traceback

from tornado.ioloop import IOLoop
from tornado.httpclient import AsyncHTTPClient
from tornado.web import Application, RequestHandler, HTTPError
from tornado.websocket import WebSocketHandler
from tornado.escape import json_encode, json_decode


API_URL = 'https://app.krustydankmeme.com/ajax/get_session'
LISTEN_HOST = '0.0.0.0'
LISTEN_PORT = 8080


SOCKETS = defaultdict(list)

async def get_user(token):
    client = AsyncHTTPClient()
    resp = await client.fetch(
        API_URL, 
        method='POST',
        headers={'Content-Type': 'application/json'},
        body=json_encode({'token': token})
    )
    resp.rethrow()
    return json_decode(resp.body)



class SendHandler(RequestHandler):
    def post(self):
        if self.request.headers['Content-Type'] != 'application/json':
            raise HTTPError(400)

        cation = json_decode(self.request.body)
        for ws in SOCKETS[cation['receiver']]:
            try:
                ws.write_message(json_encode({
                    'type': 'notification',
                    'notification': cation
                }))
            except:
                traceback.print_exc()
        self.set_status(204)


class PushHandler(WebSocketHandler):
    def initialize(self):
        self.username = None

    def open(self):
        print('New connection')
    
    async def on_message(self, message):
        if not message or message == 'close':
            self.close()
        
        msg = json_decode(message)
        
        if msg['type'] == 'register':
            user = await get_user(msg['token'])
            self.username = user['username']
            SOCKETS[self.username].append(self)
            self.write_message(json_encode({'type': 'ready'}))

    def on_close(self):
        try:
            SOCKETS[self.username].remove(self)
        except ValueError:
            pass
    
    def check_origin(self, origin):
        return True


if __name__ == '__main__':
    app = Application([
        (r'/send', SendHandler),
        (r'/push', PushHandler)
    ])
    app.listen(8080)
    IOLoop.current().start()

