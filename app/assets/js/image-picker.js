!function() {
    var setPreview = function(el, objUrl) {
        el.style.backgroundImage = objUrl ? "url(" + objUrl + ")" : null;
    };
    
    var getDataUrl = function(file, callback) {
        var reader = new FileReader();
        reader.onload = function() {
            callback(reader.result);
        };
        reader.readAsDataURL(file);
    };
    
    window.initImagePicker = function(app, el, props) {
        var objUrl = null;
        var fileInput = document.createElement("input");
        fileInput.type = "file";
        fileInput.style.display = "none";
        
        fileInput.addEventListener("change", function() {
            var file = fileInput.files[0];
            objUrl && URL.revokeObjectURL(objUrl);
            objUrl = URL.createObjectURL(file);
            setPreview(el, objUrl);
            props.onChange && getDataUrl(file, props.onChange);
        });
        
        el.addEventListener("click", function() {fileInput.click()});
    };
}();
