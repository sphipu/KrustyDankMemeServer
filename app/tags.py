from .main import db
from .models import Hashtag


def find_tags(s, sep=' ', prefix='#'):
    """Get a tuple is_tag, word for every word"""
    words = s.split(sep)
    for word in words:
        is_tag = word.startswith(prefix)
        yield is_tag, word


def get_tags(s, sep=' ', prefix='#', exclude_prefix=True):
    """Get a list with all tags found in s"""
    for is_tag, word in find_tags(s, sep=sep, prefix=prefix):
        if is_tag:
            if exclude_prefix:
                yield word[len(prefix):]  # remove the prefix
            else:
                yield word



def save_post_tags(text, post_id):
    for tag in get_tags(text):
        tag = Hashtag(text=tag, post_id=post_id)
        db.session.add(tag)

