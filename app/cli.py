import os
import uuid
import click
from getpass import getpass
from .main import db
from .models import User
from .auth import hash_password


def cli_init_app(app):
    @app.cli.command()
    @click.argument('name')
    def create_admin(name):
        password = getpass()
        admin = User(
            username=name, email='%s@localhost' % name,
            first_name='Admin', last_name='Admin',
            is_admin=True, is_enabled=True,
            address='localhost', zip_code=127001,
            password_hash=hash_password(name, password)
        )
        
        db.session.add(admin)
        db.session.commit()

