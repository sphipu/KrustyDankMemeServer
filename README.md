# KDM Server
Krusty dank meme server is a private REST-like API to power the Krusty Dank Meme app. 

# Setup
KDM Server is ment to run on linux or windows via WSGI. The easyest deployment method is to user uWSGI and NGINX. To install a basic uWSGI environment use the following stepps: 

1. Create a folder where the application files live
2. Create a folder where the images are stored
3. Clone the git repository
4. Create a virtual environment
5. Install the required packages
6. execute "flask init_instance" and "flask db upgrade"

