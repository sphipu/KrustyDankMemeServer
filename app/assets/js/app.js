!function() {
    "use strict";
    var assignEvents = function(app, el, props) {
        var header = el.querySelector(".header"),
            actionMenu = el.querySelector(".action-menu"),
            profileMenu = el.querySelector(".profile-menu"),
            actionButton = header.querySelector(".action-button"),
            actionCloseButton = actionMenu.querySelector(".close-button"),
            profileButton = header.querySelector(".profile-button"),
            logoutButton = profileMenu.querySelector(".logout-button"),
            notificationBadges = app.helpers.getNotificationBadges(app, el),
            notificationPane = el.querySelector(".single-notification"),
            notificationPaneClose = notificationPane.querySelector("a"),
            notificationText = notificationPane.querySelector(".text");
        
        
        var showNotification = function(text, duration) {
            duration = duration || 10000;
            notificationText.innerHTML = text;
            notificationPane.classList.add("visible");
            setTimeout(function() {
                notificationPane.classList.remove("visible");
            }, duration);
        };
        
        actionButton.addEventListener("click", function(e) {
            e.stopPropagation();
            actionMenu.classList.add("visible");
            profileMenu.classList.remove("visible");
        });
        
        actionCloseButton.addEventListener("click", function(e) {
            actionMenu.classList.remove("visible");
        });

        profileButton.addEventListener("click", function(e) {
            e.stopPropagation();
            profileMenu.classList.toggle("visible");
            actionMenu.classList.remove("visible");
        });
        
        el.addEventListener("click", function(e) {
            actionMenu.classList.remove("visible");
            profileMenu.classList.remove("visible");
        });
        
        logoutButton && logoutButton.addEventListener("click", function(e) {
            e.preventDefault();
            app.tryFetchContent("POST", "ajax.logout", null, function(res) {
                window.location.href = "/";
            });
        });
        
        app.addEventListener("notification", function(e) {
            notificationBadges.forEach(function(el) { el.setAmount(e.detail.unreadAmount) });
        });

        window.addEventListener("scroll", function(e) { app.dispatchEvent("scroll", e)});
            
        app.addEventListener("scroll", function(e) {
            var scrollTop = e.target.scrollTop || e.target.scrollingElement.scrollTop;
            if (scrollTop > 30) {
                header.classList.remove("header-big")
            }
            else {
                header.classList.add("header-big");
            }
        });
        
        notificationPaneClose.addEventListener("click", function(e) {
            e.preventDefault();
            notificationPane.classList.remove("visible");
        });
        
		if (WebSocket && props.pushServer && props.pushToken) {
			// Assign push events
			var ws = new WebSocket(props.pushServer);
			
			ws.addEventListener("open", function(e) {
				ws.send(JSON.stringify({
					type: "register",
					token: props.pushToken
				}));
				console.log("WS Register sent");
			});
			
			ws.addEventListener("message", function(e) {
				var msg = JSON.parse(e.data);
				if (msg.type == "ready") {
					console.log("WebSocket connection established and ready to receive updates");
					return;
				}
				
				if (msg.type == "notification") {
					var cation = msg.notification;
					
					app.dispatchEvent("notification", new CustomEvent("notificationAmountUpdate", {detail: {
						unreadAmount: cation["notification_amount"]
					}}));
					
					showNotification(cation.text);
				}
			});
			
			document.addEventListener("beforeunload", function() {
				ws.send("close");
			});
		}
        else if (props.pushToken === false) {
            // No push needed on this page
            console.log('We need no push');
        }
		else {
			// No websockets are available or not configured right
			showNotification("Push notifications are not supported");
		}
        
    };
    
    var NotificationBadge = function(app, el) {
        this._amountEl = el.querySelector("span.amount");
        this._el = el;
    };
    
    NotificationBadge.prototype.setAmount = function(amount) {
        this._amountEl.innerHTML = amount;
        if (amount) {
            this._el.classList.remove("hidden");
        }
        else {
            this._el.classList.add("hidden");
        }
    };

    var helpers = {
        /**
         * Ensure that the given value is true, else raise
         * an Exception
         **/
        ensureSet: function(value, message) {
            if (!value) {
                var msg = message || "The value must be set";
                throw new Error(msg);
            }
            return value;
        },
        
        ensureInitOnce: function (el, hasClass) {
            if (hasClass && !el.classList.contains(hasClass)) {
                throw new Error("Can only init items with class " + hasClass);
            }
            
            if (el.dataset.init === "true") {
                throw new Error("This item is allready initialized");
            }
            
            el.dataset.init = "true";
        },
        
        getNotificationBadges: function(app, el) {
            var badges = el.querySelectorAll(".notification-badge");
            var badgeObjects = [];
            
            for (var i = 0; i < badges.length; i++) {
                badgeObjects.push(new NotificationBadge(app, badges[i]));
            }
            return badgeObjects;
        },
        listRemove: function(list, item) {
            var index = list.indexOf(item);
            if (index > -1) {
                list.splice(index, 1);
            }
            else {
                throw new Error("Failed to remove " + item + " from list, because it doesn't exist");
            }
        }
    };
    
    /**
     * Fetch content from the server. 
     * method: The HTTP method ("GET", "POST", etc..)
     * endpoint: The API endpoint ("ajax.like_post" for example)
     * data: The data / post body of the request
     * onSuccess: a function with one parameter, which will be the parsed json response
     * config: an object with config values:
     * - onError: a function with two arguments, the first is a error object, 
     *   the second a human readable error message. 
     * - onProgress: a function with one event parameter, called on download progress
     * - onUploadProgress: a function with one event parameter called on upload progress
     * */
    var fetchContent = function(method, endpoint, data, callback, config) {
        var ajaxEndpoints = helpers.ensureSet(config.ajaxEndpoints),
            csrfToken = helpers.ensureSet(config.csrfToken),
            url = helpers.ensureSet(ajaxEndpoints[endpoint], "Invalid ajax endpoint " + endpoint),
            xhttp = new XMLHttpRequest();

        xhttp.onprogress = config.onProgress;
        xhttp.upload.onprogress = config.onUploadProgress;
        
        xhttp.onloadend = function() {
            if (xhttp.readyState != 4) return; // Only handle ready state 4
            
            var contentType = xhttp.getResponseHeader("Content-Type"),
                data = null;
            
            if (contentType !== "application/json") {
                console.warn(xhttp.responseText);
                var err = new Error("Invalid content type");
                return callback(null, err);
            }
            
            data = JSON.parse(xhttp.responseText);
            
            if (data && data.error) {
                var err = new Error("The server sent the error " + data.error);
                return callback(null, err, data.error);
            }
            
            if (xhttp.status < 200 || xhttp.status >= 400) {
                console.warn(xhttp.responseText);
                var err = new Error("Invalid HTTP status " + xhttp.status);
                return callback(null, err);
            }
            
            return callback(data);
        };
        
        xhttp.onerror = function(err) {callback(null, err)};
        xhttp.open(method, url);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.setRequestHeader("X-CSRFToken", csrfToken);
        xhttp.send(JSON.stringify(data));
    };
    
    
    var App = function(el, props) {
        this.helpers = helpers;
        this.helpers.ensureSet(props.csrfToken);
        this.helpers.ensureSet(props.ajaxEndpoints);
        this._ajaxEndpoints = props.ajaxEndpoints;
        this._csrfToken = props.csrfToken;
        this._events = {};

        assignEvents(this, el, props);
        
        this.dispatchEvent("notification", new CustomEvent("notificationInit", {detail: {
            unreadAmount: props.notificationAmount
        }}));
    };
    
    App.prototype._getFetchConfig = function(config) {
        var cfg = config || {};
        cfg.ajaxEndpoints = this._ajaxEndpoints;
        cfg.csrfToken = this._csrfToken;
        return cfg;
    };
    
    App.prototype.fetchContent = function(method, endpoint, data, callback, config) {
        return fetchContent(method, endpoint, data, callback, this._getFetchConfig(config));
    };
    
    App.prototype.tryFetchContent = function(method, endpoint, data, successCallback, config) {

        fetchContent(method, endpoint, data, function(res, err, errMsg) {
            if (res) return successCallback(res);
            // Handle the error
            var msg = errMsg || "Server communication failed";
            console.warn(err);
            alert(msg);
        }, this._getFetchConfig(config));
    };
    
    App.prototype.addEventListener = function(eventName, callback) {
        var lst = this._events[eventName] = this._events[eventName] || [];
        lst.push(callback);
    };
    
    App.prototype.dispatchEvent = function(eventName, eventArg) {
        var lst = this._events[eventName] || [];
        lst.forEach(function(cb) {
            try {
                cb(eventArg);
            }
            catch (err) {
                console.warn(err);
            }
        });
    };
    
    App.prototype.getEndpointUrl = function(endpoint) {
        return this.helpers.ensureSet(this._ajaxEndpoints[endpoint], "Invalid endpoint");
    }
    
    window.initApp = function(props) {
        if (window.withApp) return;
        var app = new App(document.body, props);
        window.withApp = function(fn) {fn(app)};
    };
}();


