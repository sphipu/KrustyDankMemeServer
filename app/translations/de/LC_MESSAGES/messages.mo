��    %      D              l     m     }     �     �     �     �  	   �     �     �               %     B     I     Q  
   Y     d     q     z     �     �  
   �     �     �     �  	   �  	   �     �     �       )     %   A     g  
   w  
   �     �  �  �     *  /   ?  	   o     y     �     �     �     �     �  
          &   '     N     W     `     l     x     �     �  "   �  	   �  
   �  	   �     �  %   �  	             +  /   C     s  0   �  %   �     �     �     �     	   404 - Not found A user with this mail exists Approve Approve posts Confirm new password Create your account Edit post Edit your personal data Edit your profile Gender Invalid password Invalid username or password Log in Log out My info My profile New Password New post Password Passwords must match Posts Profile of Save Sign up Sign up for a new account Subscribe User info User not found User with this name exists Username We can't find what you are looking for :/ You are not allowed to view this page You may want to Your posts [Approved] [Not approved] Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2018-10-25 11:42+0200
PO-Revision-Date: 2018-10-25 11:05+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 404 - Nicht gefunden Ein Benutzer mit diser e-Mail existiert bereits Freigeben Beiträge freigeben Neues Passwort bestätigen Dein Account erstellen Beitrag bearbeiten Persönliche Daten bearbeiten Dein Profil bearbeiten Geschlecht Ungültiges Passwort Ungültiger Benutzername oder Passwort Anmelden Abmelden Deine Infos Dein Profil Neues Passwort Neuer Beitrag Passwort Passwörter müssen identisch sein Beiträge Profil von Speichern Registrieren Für einen neuen Account registrieren Abonieren Benutzer Info Benutzer nicht gefunden Ein Benutzer mit diesem Namen existiert bereits Benutzername Wir können leider nicht finden was du suchst :/ Du darfst diese Seite nicht anschauen Eventuell möchtest du Deine Beiträge [Freigegeben] [Nicht freigegeben] 