import os
import uuid
from base64 import b64encode, b64decode
import PIL
from imgpy import Img
import io
from flask import current_app


def dataurl_to_base64(dataurl):
    schema, dataurl = dataurl.split(':')
    assert schema == 'data'
    mimetype, dataurl = dataurl.split(';')
    b64_str, base64 = dataurl.split(',')
    assert b64_str == 'base64'
    return mimetype, base64


def base64_to_dataurl(mimetype, base64):
    dataurl = 'data:%s;base64,%s' % (mimetype, base64)
    return dataurl


def get_image_name(image_id, ext='jpg'):
    return '%s.%s' % (image_id, ext)


def get_image_path(image_name):
    image_path = os.path.abspath(current_app.config['IMAGE_PATH'])
    return os.path.join(image_path, image_name)


def save_image(dataurl):
    mimetype, b64 = dataurl_to_base64(dataurl)
    image_io = io.BytesIO(b64decode(b64))
    
    with Img(fp=image_io) as img:
        ext = 'gif' if len(img.frames) > 0 else 'jpg'
        image_name = get_image_name(str(uuid.uuid4()), ext)
        path = get_image_path(image_name)
        if img.size[1] > 1024:
            img.thumbnail((1024, 768))
        if ext == 'gif':
            img.save(fp=path)
        else:
            img.convert('RGB').save(fp=path)
    return image_name


def get_image(image_name):
    path = get_image_path(image_name)
    with open(path, 'rb') as fp:
        b64 = b64encode(fp.read()).decode('utf-8')
    return base64_to_dataurl('image/jpeg', b64)


def delete_image(image_id):
    path = get_image_path(image_id)
    os.remove(path)

