!function() {
    "use strict";

    window.initPostEditor = function(app, el, props) {
        var onSave = app.helpers.ensureSet(props.onSave, "onSave callback is neccessary");
        app.helpers.ensureInitOnce(el, "post-editor");
        
        var postDiv = el.querySelector(".post"),
            id = parseInt(postDiv.dataset.postid),
            submitButton = postDiv.querySelector("button.submit-button"),
            textInput = postDiv.querySelector("textarea.text-input"),
            progressBar = postDiv.querySelector("div.progress-bar"),
            progressSlider = progressBar.querySelector(".slider"),
            previewElement = postDiv.querySelector("div.image-preview"),
            image = null;
            
        var updateSubmitEnableState = function() {
            var isSubmitable;
            if (id == 0) {
                // we have a new post
                isSubmitable = image && textInput.value;
            }
            else {
                isSubmitable = textInput.value;
            }
            
            submitButton.disabled = isSubmitable ? null : "disabled";
        };
        
        if (id == 0) {
            // we have a new post
            window.initImagePicker(app, previewElement, {
                onChange: function(dataUrl) {
                    image = dataUrl;
                    updateSubmitEnableState();
                }
            });
        }
        
        el.addEventListener("submit", function(e) {
            e.preventDefault();
            submitButton.disabled = "disabled"; //Avoid double submission
            var data = {
                "id": id,
                "text": textInput.value,
                "image": image
            };
            
            app.tryFetchContent("POST", "ajax.save_post", data, onSave, {
                onUploadProgress: function(e) {
                    var percent = e.loaded * 100 / e.total;
                    progressBar.classList.remove("hidden");
                    progressBar.style.width = percent + "%";
                }
            });
        });
        
        textInput.addEventListener("input", updateSubmitEnableState);
        
    };
}();
