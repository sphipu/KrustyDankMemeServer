"""empty message

Revision ID: 7b31a2c13457
Revises: ad3921865a2a
Create Date: 2018-11-16 13:48:01.991190

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7b31a2c13457'
down_revision = 'ad3921865a2a'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'notification_amount')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('notification_amount', sa.INTEGER(), nullable=True))
    # ### end Alembic commands ###
