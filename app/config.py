import os
import json
import uuid


def get_default_config(app):
    db_path = os.path.join(app.instance_path, 'db.sqlite3')
    return {
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///%s' % db_path,
        'SECRET_KEY': str(uuid.uuid4()),
        'IMAGE_PATH': os.path.join(app.instance_path, 'images')
    }

INSTANCE_DIRS = [
    "cache",
]

def config_init_app(app):
    settings_fname = os.path.join(app.instance_path, 'settings.json')
    
    if not os.path.isdir(app.instance_path):
        os.mkdir(app.instance_path)
    
    if os.path.isfile(settings_fname):
        with open(settings_fname) as f:
            config = json.load(f)
    else:
        config = get_default_config(app)
        
        #save the config
        with open(settings_fname, 'w') as f:
            json.dump(config, f)

    app.config.update(config)
    
    if not os.path.isdir(config['IMAGE_PATH']):
        os.makedirs(config['IMAGE_PATH'], exist_ok=True)
    
    for d in INSTANCE_DIRS:
        os.makedirs(os.path.join(app.instance_path, d), exist_ok=True)
