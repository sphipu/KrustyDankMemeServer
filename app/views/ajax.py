from datetime import datetime
import json
import time
from ..main import csrf

from flask import (
    Blueprint, request, g, jsonify, url_for, escape, get_template_attribute,
    abort, Response, stream_with_context
)
from flask_babel import gettext as _

from ..models import (
    Reaction, Post, Comment, Hashtag, User, UserSubscription,
    Notification, NotificationType as NT, Session
)
from ..auth import login_required
from ..images import save_image
from ..tags import save_post_tags
from ..main import db
from ..push import send_notification


bp = Blueprint('ajax', __name__)


def add_coins(receiver, triggered_by=None, amount=10):
    if triggered_by is None:
        triggered_by = g.user

    if receiver.id != triggered_by.id:
        if receiver.coins + amount < 0:  # never less than 0 coins
            receiver.coins = 0

        receiver.coins = receiver.coins + amount


@bp.route('/post_toggle_like', methods=['POST'])
@login_required
def like_post():
    post = Post.query.filter_by(id=request.json['post_id']).one()
    reaction = Reaction.query.filter_by(
        author_id=g.user.id, post=post).first()

    if reaction is None:
        reaction = Reaction(type=1)
        reaction.post_id = post.id
        reaction.author_id = g.user.id
        add_coins(post.author, amount=10)
        g.user.coins += 1
        db.session.add(reaction)
        is_liked = True
        send_notification(post.author_id, NT.like_post)
    else:
        db.session.delete(reaction)
        add_coins(post.author, amount=-10)
        g.user.coins -= 1
        is_liked = False
        send_notification(post.author_id, NT.unlike_post)

    db.session.add(post.author)
    db.session.add(g.user)
    db.session.commit()
    amount = Reaction.query.filter_by(post_id=post.id, type=1).count()
    return jsonify({'like_amount': amount, 'is_liked': is_liked})


@bp.route('comment_toggle_like', methods=['POST'])
@login_required
def like_comment():
    comment = Comment.query.filter_by(id=request.json['comment_id']).one()
    reaction = Reaction.query.filter_by(
        author_id=g.user.id, comment=comment).first()

    if reaction is None:
        reaction = Reaction(type=1)
        reaction.comment_id = comment.id
        reaction.author_id = g.user.id
        add_coins(comment.author, amount=5)
        g.user.coins += 1
        db.session.add(reaction)
        is_liked = True
        send_notification(comment.author_id, NT.like_comment)
    else:
        db.session.delete(reaction)
        g.user.coins -= 1
        add_coins(comment.author, amount=-5)
        is_liked = False
        send_notification(comment.author_id, NT.unlike_comment)

    db.session.add(comment.author)
    db.session.add(g.user)
    db.session.commit()
    amount = Reaction.query.filter_by(comment_id=comment.id, type=1).count()
    return jsonify({'like_amount': amount, 'is_liked': is_liked})


@bp.route('/logout', methods=['POST'])
@login_required
def logout():
    db.session.delete(g.session)
    db.session.commit()
    return jsonify({})


@bp.route('/post_toggle_approve', methods=['POST'])
@login_required
def approve_post():
    assert g.user.is_admin or g.user.is_moderator
    post_id = request.json['post_id']
    post = Post.query.filter_by(id=post_id).one()
    
    if post.approved:
        post.approved = None
        send_notification(post.author_id, NT.post_unapproved)

    else:
        post.approved = datetime.utcnow()
        send_notification(post.author_id, NT.post_approved)


    db.session.add(post)
    db.session.commit()
    return jsonify({'is_approved': post.approved})


@bp.route('/add_comment', methods=['POST'])
@login_required
def add_comment():
    post = Post.query.filter_by(id=request.json['post_id']).one()
    text = request.json['text']
    
    if not text or text.isspace():
        return jsonify({
            'error': _('Comments must not be empty')
        }), 400

    comment = Comment(post_id=post.id, text=text, author_id=g.user.id)
    add_coins(post.author, amount=50)
    g.user.coins += 5
    send_notification(post.author_id, NT.add_comment)
    
    db.session.add(comment)
    db.session.add(g.user)
    db.session.commit()
    comment_amount = Comment.query.filter_by(post_id=post.id).count()
    render_comment = get_template_attribute('posts/_macros.html',
                                            'comment')
    return jsonify({
        'comment_html': render_comment(comment),
        'comment_amount': comment_amount
    })



@bp.route('/delete_post', methods=['POST'])
@login_required
def delete_post():
    post_id = request.json['post_id']
    post = Post.query.filter_by(id=post_id).one()
    db.session.delete(post)
    db.session.commit()
    return jsonify({})


@bp.route('/get_posts', methods=['POST'])
@login_required
def get_posts():
    feed = request.json['feed']
    lp = Post.query.filter_by(id=request.json['lp']).one()

    if feed == 'all':
        query = (Post.query.filter(Post.approved != None)
				 .filter(Post.approved < lp.approved)
				 .order_by(Post.approved.desc()))

    elif feed == 'approve':
        if not (g.user.is_moderator or g.user.is_admin):
            return abort(403)

        query = (Post.query.filter(Post.approved == None)
				 .filter(Post.added < lp.added)
				 .order_by(Post.added.desc()))

    elif feed == 'yours':
        query = (Post.query.filter_by(author_id=g.user.id)
				 .filter(Post.added < lp.added)
				 .order_by(Post.added.desc()))
    else:
        return abort(400)
    
    posts = query.limit(3)
    render_post = get_template_attribute('posts/_macros.html', 'post')
    
    return jsonify({
        'has_more': posts.count() == 3,
        'posts_html': [render_post(i) for i in posts[0:2]]
    })


@bp.route('/user_toggle_subscription', methods=['POST'])
@login_required
def subscribe_user():
    user_id = request.json['user_id']
    subscription = UserSubscription.query.filter_by(follows_id=user_id).first()
    if subscription is None:
        is_subscribed = True
        subscription = UserSubscription(follows_id=user_id, user_id=g.user.id)
        send_notification(user_id, NT.user_subscribed)
        db.session.add(subscription)
    else:
        is_subscribed = False
        send_notification(user_id, NT.user_unsubscribed)
        db.session.delete(subscription)

    db.session.commit()
    user = User.query.filter_by(id=user_id).one()
    return jsonify({'is_subscribed': is_subscribed,
                    'follower_amount': user.follower_amount})


@bp.route('/save_post', methods=['POST'])
@login_required
def save_post():
    data = request.json
    if data.get('id', 0):
        # We're editing a post
        if not data['text']:
            abort(400)

        post = Post.query.filter_by(id=data['id']).one()
        post.text = data['text']
        post.modified = datetime.utcnow()
        Hashtag.query.filter_by(post_id=post.id).delete()
        save_post_tags(post.text, post.id)
        db.session.add(post)
        db.session.commit()
        return jsonify({})
    else:
        # We're uploading a new post
        if not data['text'] or not data['image']:
            abort(400)

        post = Post(text=data['text'], author=g.user)
        post.image_name = save_image(data['image'])
        g.user.last_post = datetime.utcnow()
        db.session.add(post)
        db.session.add(g.user)
        db.session.commit()
        save_post_tags(post.text, post.id)
        db.session.commit()
        return jsonify({})


@bp.route('/get_comments')
@login_required
def get_comments():
    lc = Comment.query.filter_by(id=request.json['lc']).one()
    raise NotImplemented


@bp.route('/read_notification', methods=['POST'])
@login_required
def read_notification():
    cation = Notification.query.filter_by(
        id=request.json['notification_id']).one()
    cation.is_read = not cation.is_read  # toggle
    db.session.add(cation)
    db.session.commit()
    return jsonify({
        'is_read': cation.is_read,
        'unread_notifications': g.user.notification_amount
    })

@bp.route('/get_session', methods=['POST'])
@csrf.exempt
def get_session():
    token = request.json['token']
    session = Session.query.filter_by(token=token).first()

    if not session:
        return jsonify({
            'error': 'Session not found'
        }), 404

    return jsonify({
        'username': session.user.username,
        'user_id': session.user.id,
        'notification_amount': session.user.notification_amount
    })


@bp.errorhandler(403)
def handle_403(err):
    print(err)
    return jsonify({'error': str(err)})
