from .auth import hash_password



def copy_user_in_form(user, form):
    form.username.data = user.username
    form.email.data = user.email
    form.first_name.data = user.first_name
    form.last_name.data = user.last_name
    form.address.data = user.address
    form.zip_code.data = user.zip_code
    form.town.data = user.town
    form.birthday.data = user.birthday
    form.gender.data = user.gender
    form.coins.data = user.coins


def copy_form_in_user(form, user):
    username_changed = user.username != form.username.data
    user.username = form.username.data
    user.email = form.email.data
    user.first_name = form.first_name.data
    user.last_name = form.last_name.data
    user.address = form.address.data
    user.zip_code = form.zip_code.data
    user.town = form.town.data
    user.birthday = form.birthday.data
    user.gender = form.gender.data

    if form.password.data:
        user.password_hash = hash_password(user.username, form.password.data)

    elif username_changed:
        pwd_hash = hash_password(user.username, form.old_password.data)
        user.password_hash = pwd_hash
