from flask import g
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, IntegerField, PasswordField, DateField, \
     HiddenField
from wtforms.fields.html5 import EmailField
from wtforms.widgets import HiddenInput, TextArea
from wtforms.validators import DataRequired, ValidationError, EqualTo
from flask_babel import lazy_gettext as _l

from .auth import hash_password
from .models import User


class LoginForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    password = PasswordField(_l('Password'), validators=[DataRequired()])

    def validate_username(self, field):
        #check if user exists
        user = User.query.filter_by(username=field.data).first()
        if not user:
            raise ValidationError(_l('User not found'))

    def validate_password(self, field):
        username = self.username.data
        # check if password is correct
        user = User.query.filter_by(
            username=username,
            password_hash=hash_password(username, field.data)).first()

        if not user:
            raise ValidationError(_l('Invalid username or password'))


class UserForm(FlaskForm):
    username = StringField('Username', [DataRequired()])
    email = EmailField('Email', [DataRequired()])
    first_name = StringField('First name', [DataRequired()])
    last_name = StringField('Last name', [DataRequired()])
    address = StringField('Address', [DataRequired()])
    zip_code = IntegerField('Zip code', [DataRequired()])
    town = StringField('Town', [DataRequired()])
    birthday = DateField('Birthday', default=None)
    coins = IntegerField('Coins', default=0)
    password = PasswordField(
        _l('Password'),
        [DataRequired(),
         EqualTo('password_confirm', message=_l('Passwords must match'))])
    
    password_confirm = PasswordField('Confirm password', [DataRequired()])
    gender = StringField(_l('Gender'), [DataRequired()])


    def validate_username(self, field):
        user = User.query.filter_by(username=field.data).first()

        if user is not None:
            raise ValidationError(_l('User with this name exists'))

    def validate_email(self, field):
        user = User.query.filter_by(email=field.data).first()
        if user is not None:
            raise ValidationError(_l('A user with this mail exists'))



class EditUserForm(UserForm):
    old_password = PasswordField(_l('Password'))
    password = PasswordField(
        _l('New Password'),
        [EqualTo('password_confirm', message=_l('Passwords must match'))])
    password_confirm = PasswordField(_l('Confirm new password'))

    def validate_old_password(self, field):
        if g.user.password_hash != hash_password(g.user.username, field.data):
            raise ValidationError(_l('Invalid password'))

    def validate_username(self, field):
        if field.data != g.user.username:
            UserForm.validate_username(self, field)

    def validate_email(self, field):
        if field.data != g.user.email:
            UserForm.validate_email(self, field)



class ProfileForm(FlaskForm):
    header_image = HiddenField()
    profile_image = HiddenField()
    user_info = StringField(_l('User info'), widget=TextArea())
