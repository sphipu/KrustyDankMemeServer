from flask import Blueprint, send_file
from ..images import get_image_path

bp = Blueprint('images', __name__)


@bp.route('/get/<string:image_name>')
def get(image_name):
    path = get_image_path(image_name)
    return send_file(path)
