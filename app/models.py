from datetime import datetime
import uuid
import enum
from flask import g, url_for
from flask_babel import gettext as _
from .main import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False, index=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    first_name = db.Column(db.String(80), nullable=False)
    last_name = db.Column(db.String(80), nullable=False)
    address = db.Column(db.String(120), nullable=False)
    zip_code = db.Column(db.Integer, nullable=False)
    town = db.Column(db.String(80), nullable=True)
    birthday = db.Column(db.DateTime, nullable=True)
    email_confirmed = db.Column(db.Boolean, nullable=False, default=False)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    is_moderator = db.Column(db.Boolean, nullable=False, default=False)
    coins = db.Column(db.Integer, nullable=False, default=0)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_login = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_post = db.Column(db.DateTime, nullable=True)
    password_hash = db.Column(db.String(70), nullable=False)
    is_enabled = db.Column(db.Boolean, nullable=False)
    profile_image_name = db.Column(db.String(40), nullable=True)
    header_image_name = db.Column(db.String(40), nullable=True)
    gender = db.Column(db.String(10), nullable=True)
    user_info = db.Column(db.String(1024))

    @property
    def profile_image_url(self):
        if self.profile_image_name:
            return url_for('images.get', image_name=self.profile_image_name)
        return None

    @property
    def header_image_url(self):
        if self.header_image_name:
            return url_for('images.get', image_name=self.header_image_name)
        return None

    @property
    def profile_url(self):
        return url_for('users.profile_of', username=self.username)

    @property
    def is_subscribed(self):
        subscription = UserSubscription.query.filter_by(
            user_id=g.user.id, follows_id=self.id).count()
        return subscription == 1

    @property
    def follower_amount(self):
        return UserSubscription.query.filter_by(follows_id=self.id).count()

    @property
    def total_like_amount(self):
        post_ids = [i.id for i in Post.query.filter_by(author_id=self.id).all()]
        comment_ids = [i.id for i in
                       Comment.query.filter_by(author_id=self.id).all()]
        return Reaction.query.filter(
            (Reaction.comment_id.in_(comment_ids))
            | (Reaction.post_id.in_(post_ids))).count()

    @property
    def post_amount(self):
        return Post.query.filter_by(author_id=self.id).count()


    @property
    def notifications(self):
        return Notification.query.order_by(Notification.added.desc()).all()

    @property
    def notification_amount(self):
        return (Notification.query
                .filter_by(receiver_id=self.id, is_read=False)
                .count()
        )



class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    token = db.Column(db.String(40), nullable=False,
                      default=lambda: str(uuid.uuid4()))
    user = db.relationship('User', backref=db.backref('sessions'), lazy=True)



class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    image_name = db.Column(db.String(40), nullable=False)
    text = db.Column(db.String(1024), nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    modified = db.Column(db.DateTime, nullable=True)
    approved = db.Column(db.DateTime, nullable=True)
    author = db.relationship('User', backref=db.backref('posts'), lazy=True)

    @property
    def like_amount(self):
        return Reaction.query.filter_by(post_id=self.id, type=1).count()

    @property
    def is_liked(self):
        like = Reaction.query.filter_by(
            author_id=g.user.id, post_id=self.id).first()
        return like is not None

    @property
    def is_editable(self):
        return g.user.id == self.author_id

    @property
    def comment_amount(self):
        return Comment.query.filter_by(post_id=self.id).count()

    @property
    def image_url(self):
        return url_for('images.get', image_name=self.image_name)

    @property
    def edit_url(self):
        return url_for('posts.edit', post_id=self.id)


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(1024), nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    modified = db.Column(db.DateTime, nullable=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('comment.id'),
                          nullable=True)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=False)
    author = db.relationship(
        'User', backref=db.backref('comments', order_by="Comment.added.desc()"),
        lazy=True)
    post = db.relationship(
        'Post', backref=db.backref('comments', order_by="Comment.added.desc()",
                                   cascade="delete"),
        lazy=True)

    @property
    def like_amount(self):
        return Reaction.query.filter_by(comment_id=self.id, type=1).count()

    @property
    def is_liked(self):
        like = Reaction.query.filter_by(
            author_id=g.user.id, comment_id=self.id).first()
        return like is not None


class Reaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer, nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    modified = db.Column(db.DateTime, nullable=True)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'), nullable=True)
    comment_id = db.Column(db.Integer, db.ForeignKey('comment.id'),
                           nullable=True)
    post = db.relationship('Post', backref=db.backref('reactions'), lazy=True)
    comment = db.relationship('Comment', backref=db.backref('reactions'),
                              lazy=True)


class Hashtag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(80), nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    post = db.relationship('Post', backref=db.backref('hashtags'), lazy=True)



class Invite(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    token = db.Column(db.String(40), nullable=False,
                      default=lambda: str(uuid.uuid4()))
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    used_by_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    used = db.Column(db.DateTime, nullable=True)
    author = db.relationship('User', backref=db.backref('invites'), lazy=True,
                             foreign_keys=[author_id])
    used_by = db.relationship('User', lazy=True, foreign_keys=[used_by_id])



class UserSubscription(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    follows_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                           primary_key=True)


class NotificationType(enum.Enum):
    like_post = 1
    unlike_post = 2
    like_comment = 3
    unlike_comment = 4
    add_comment = 5
    delete_comment = 6
    mention = 7
    add_post = 8
    user_subscribed = 9
    user_unsubscribed = 10
    post_approved = 11
    post_unapproved = 12

NT = NotificationType


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Enum(NotificationType))
    trigger_id = db.Column(
        db.Integer, db.ForeignKey('user.id'),
        nullable=False,
        default=lambda: g.user.id
    )
    receiver_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                            nullable=False)
    added = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    is_read = db.Column(db.Boolean, nullable=False, default=False)
    trigger = db.relationship('User', lazy=True, foreign_keys=[trigger_id])
    receiver = db.relationship('User', lazy=True, foreign_keys=[receiver_id])
    
    @property
    def text(self):
        if self.type == NT.like_post:
            return _('%(user)s liked your post', user=self.trigger.username)
        elif self.type == NT.unlike_post:
            return _("%(user)s doesn't like your post anymore",
                     user=self.trigger.username)
        elif self.type == NT.add_comment:
            return _("%(user)s commented your post", user=self.trigger.username)
        elif self.type == NT.post_approved:
            return _("%(user)s approved your post", user=self.trigger.username)
        else:
            return str(self.type)

    
