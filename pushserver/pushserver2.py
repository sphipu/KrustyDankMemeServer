import sys
import json
import traceback
from collections import defaultdict
from flask import Flask, request
from flask_sockets import Sockets
import requests

API_URL = 'https://app.krustydankmeme.com/ajax/get_session'
LISTEN_HOST = '0.0.0.0'
LISTEN_PORT = 8080


app = Flask(__name__)
sockets = Sockets(app)


SOCKETS = defaultdict(list)

def get_user(token):
    resp = requests.post(API_URL, json={'token': token})
    resp.raise_for_status()
    return resp.json()


@app.route('/send', methods=['POST'])
def send():
    cation = request.json

    for ws in SOCKETS[cation['receiver']]:
        try:
            ws.send(json.dumps({
                'type': 'notification',
                'notification': cation
            }))
        except:
            traceback.print_exc()

    return '', 204

@sockets.route('/push')
def push_socket(ws):
    username = None
    while not ws.closed:
        msg = ws.receive()
        if not msg or msg == 'close':
            ws.close()
            break

        message = json.loads(msg)
        if message['type'] == 'register':
            token = message['token']
            user = get_user(token)
            username = user['username']
            SOCKETS[username].append(ws)
            ws.send(json.dumps({'type': 'ready'}))

    try:
        SOCKETS[username].remove(ws)
    except ValueError:
        pass

    print('Connection closed', file=sys.stderr)


if __name__ == '__main__':
    from gevent import pywsgi
    from geventwebsocket.handler import WebSocketHandler
    server = pywsgi.WSGIServer(
        (LISTEN_HOST, LISTEN_PORT),
        app, handler_class=WebSocketHandler
    )
    print('Running on %s:%d' % (LISTEN_HOST, LISTEN_PORT), file=sys.stderr)
    server.serve_forever()
