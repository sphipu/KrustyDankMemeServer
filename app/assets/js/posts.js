!function() {
    "use strict";
    var initComment = function(app, el) {
        app.helpers.ensureInitOnce(el, "comment");
        var id = el.dataset.commentid;
        var likeButton = el.querySelector("button.like-comment");
        var likeAmount = likeButton.querySelector("span.amount");
        
        likeButton.addEventListener("click", function() {
            app.tryFetchContent("POST", "ajax.like_comment", {"comment_id": id}, function(res) {
                likeAmount.innerHTML = res["like_amount"];
                if (res["is_liked"]) likeButton.classList.add("active");
                else likeButton.classList.remove("active");
            });
        });
    };
    
    var initCommentList = function(app, commentList) {
        var comments = commentList.querySelectorAll(".comment");
        for(var i = 0; i < comments.length; i++) {
            initComment(app, comments[i]);
        }
    };
    
    var initPost = function(app, el, props) {
        app.helpers.ensureInitOnce(el, "post");
        
        var id = el.dataset.postid,
            likeButton = el.querySelector("button.like-button"),
            likeAmount = likeButton.querySelector("span.amount"),
            approveButton = el.querySelector("button.approve-button"),
            approveButtonIcon = approveButton && approveButton.querySelector("span.icon"),
            deleteButton = el.querySelector("button.delete-button"),
            commentButton = el.querySelector("button.comment-button"),
            commentAmount = commentButton.querySelector("span.amount"),
            commentToggleButton = el.querySelector(".toggle-comment-area-button"),
            commentArea = el.querySelector(".comment-area"),
            newCommentForm = el.querySelector("form.new-comment-form"),
            newCommentInput = newCommentForm.querySelector(".new-comment-input"),
            commentList = commentArea.querySelector(".comment-list"),
            newCommentSubmit = newCommentForm.querySelector(".new-comment-submit");
        
        var commentsVisible = false;
            
        likeButton.addEventListener("click", function(e) {
            app.tryFetchContent("POST", "ajax.like_post", {"post_id": id}, function(res) {
                likeAmount.innerHTML = res["like_amount"];
                if (res["is_liked"]) likeButton.classList.add("active");
                else likeButton.classList.remove("active");
            });
        });
        
        approveButton && approveButton.addEventListener("click", function(e) {
            app.tryFetchContent("POST", "ajax.approve_post", {"post_id": id}, function(res) {
                if (res["is_approved"]) {
                    approveButtonIcon.classList.remove("img-unchecked");
                    approveButtonIcon.classList.add("img-checked");
                    approveButton.classList.add("active");
                }
                else {
                    approveButtonIcon.classList.remove("img-checked");
                    approveButtonIcon.classList.add("img-unchecked");
                    approveButton.classList.remove("active");
                }
            });
        });

        deleteButton && deleteButton.addEventListener("click", function(e) {
            app.tryFetchContent("POST", "ajax.delete_post", {"post_id": id}, function(res) {
                var event = new CustomEvent("post-deleted", {detail: {
                    postId: id
                }});
                el.remove();
                app.dispatchEvent("post-deleted", event);
            });
        });
        
        var onCommentClick = function(e) {
            if (commentsVisible) {
                commentsVisible = false;
                commentToggleButton.classList.add("img-arrow-down");
                commentToggleButton.classList.remove("img-arrow-up");
                commentButton.classList.remove("active");
                commentArea.classList.remove("slide-in");
            }
            else {
                commentsVisible = true;
                commentToggleButton.classList.add("img-arrow-up");
                commentToggleButton.classList.remove("img-arrow-down");
                commentButton.classList.add("active");
                commentArea.classList.add("slide-in");
            }
        };
        
        commentButton.addEventListener("click", onCommentClick);
        commentToggleButton.addEventListener("click", onCommentClick);
        
        newCommentForm.addEventListener("submit", function(e) {
            e.preventDefault();
            var data = {"post_id": id, "text": newCommentInput.value};
            newCommentSubmit.disabled = "disabled";
            app.tryFetchContent("POST", "ajax.add_comment", data, function(res) {
                commentList.insertAdjacentHTML("afterbegin", res["comment_html"]);
                commentAmount.innerHTML = res["comment_amount"];
                newCommentInput.value = "";
                newCommentSubmit.disabled = null;
                if (!commentsVisible) onCommentClick();
                initComment(app, commentList.firstElementChild);
            })
        });
        
        newCommentInput.addEventListener("input", function(e) {
            newCommentSubmit.disabled = newCommentInput.value ? null : "disabled";
        });
            
        initCommentList(app, commentList);
    };
    
    window.initPostList = function(app, el, props) {
        app.helpers.ensureInitOnce(el, "post-list");
        props = props || {};
        var postIds = [], 
            hasMore = true;
        
        var getLastId = function() {
            if (postIds.length == 0) return -1;
            return postIds[postIds.length -1]
        };
        
        var addPostEl = function(el) {
            initPost(app, el);
            postIds.push(el.dataset.postid);
        };
        
        var fetchPosts = function() {
            if (!hasMore || getLastId() == -1) return;
            hasMore = false;
            
            app.tryFetchContent("POST", "ajax.get_posts", {"lp": getLastId(), "feed": props.feed}, function(res) {
                hasMore = res["has_more"];
                var postsHtml = res["posts_html"];
                
                for (var i = 0; i < postsHtml.length; i++) {
                    el.insertAdjacentHTML("beforeend", postsHtml[i]);
                    addPostEl(el.lastElementChild);
                }
                
                props.fetchButton && !hasMore && props.fetchButton.classList.add("hidden");
            });            
        };
        
        app.addEventListener("scroll", function(e) {
            var scrollElement = e.target.scrollTop ? e.target : e.target.scrollingElement;
            var top = scrollElement.scrollTop + scrollElement.clientHeight;
            var height = scrollElement.scrollHeight;
            
            (top > height -300 && hasMore) && fetchPosts();
        });
        
        app.addEventListener("post-deleted", function(e) {
            app.helpers.listRemove(postIds, e.detail.postId);
        });
        
        props.fetchButton && props.fetchButton.addEventListener("click", fetchPosts);
        
        var posts = el.querySelectorAll(".post");
        for (var i = 0; i < posts.length; i++) {
            addPostEl(posts[i]);
        }

    };
}();
