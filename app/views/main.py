from flask import Blueprint, redirect, url_for, request
from ..auth import get_session



bp = Blueprint('main', __name__)

@bp.route('/')
def index():
    session = get_session()
    if session:
        return redirect(url_for('posts.index'))
    return redirect(url_for('users.login'))

