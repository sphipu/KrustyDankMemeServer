import os
from functools import lru_cache
from flask import current_app



def get_file(fname):
    print(fname)
    if os.path.isfile(fname):
        with open(fname) as fp:
            content = fp.read()
            print(content)
    else:
        content = ''

    return content


def head_fname():
    return os.path.join(current_app.instance_path, 'ad_tags_head')


def post_fname():
    return os.path.join(current_app.instance_path, 'ad_tags_post')


@lru_cache(maxsize=1)
def get_head_tags():
    fname = head_fname()
    return get_file(fname)


@lru_cache(maxsize=1)
def get_post_tags():
    fname = post_fname()
    return get_file(fname)


@lru_cache(maxsize=1)
def has_head_tags():
    return os.path.isfile(head_fname())


@lru_cache(maxsize=1)
def has_post_tags():
    return os.path.isfile(post_fname())
